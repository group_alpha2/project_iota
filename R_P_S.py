def user_choice():
    choice = input("Choose Rock, paper or scissors:\n").strip().lower()
    while choice not in ["rock","paper","scissors"]:
        choice = input("Invalid input. Please choose Rock, Paper or Scissors:\n").strip().lower()
    return choice

def computer_choice():
    comp_choice = ["rock","paper","scissors"]
    choice_index = (len(comp_choice)+1)//2
    return comp_choice[choice_index]

def winner(choice,comp_choice):
    if choice == comp_choice:
        return "TIE!!!"
    elif (choice == "rock" and comp_choice == "scissors") or \
         (choice == "paper" and comp_choice == "rock") or \
         (choice == "scissors" and comp_choice == "paper"):
        return f"YOU WIN!!! {choice} beats {comp_choice}."
    else:
        return f"COMPUTER WINS!!! {comp_choice} beats {choice}"
    
def play_game():
    print("LETS PLAY ROCK, PAPER, SCISSORS!!!!!")
    player = user_choice()
    computer = computer_choice()
    print(f"YOU CHOSE: {player}")
    print(f"COMPUTER CHOSE: {computer}")
    result = winner(player, computer)
    print(result)

play_game()