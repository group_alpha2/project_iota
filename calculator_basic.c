#include <stdio.h>
#include<stdlib.h>

void calculate(int *nums, int size, char sign){
    if (size <= 1){
        printf("Not enough numbers to perform the operation.\n");
        return;
    }double result = nums[0];
    for (int i = 1; i < size; i++){
        switch (sign){
        case '+':
            result += nums[i];
            break;
        case '-':
            result -= nums[i];
            break;
        case '*':
            result *= nums[i];
            break;
        case '/':
            if (nums[i] !=0){
            result /= nums[i];
        }else{
            printf("Zero division error.\n");
            free(nums);
            return;
        }break;
        
        default:
        printf("Invalid operation!!!\n");
        free(nums);
            return;
        }}printf("\nThe result is: %.4f.\n",result);
    free(nums);
}int main(){
    int *nums = NULL;
    int size = 0;
    int input;
    char sign;

    printf("Enter digits(symbols or characters to stop)\n");
    while (scanf("%d",&input)==1){
        size++;
        nums = realloc(nums,size*sizeof(int));
        if (nums == NULL){
            printf("not enough memory\n");
            return 1;
        }nums[size-1] = input;
    }fflush(stdin);
    printf("Enter the sign(+,-,*,/):");
    scanf(" %c",&sign);
    calculate(nums,size,sign);
    return 0;
}