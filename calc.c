#include <stdio.h>
#include <string.h>
#include <stdarg.h>

    double add(int count, ...);
    double sub(int count, ...);
    double mult(int count, ...);
    double div(int count, ...);

struct Calculator{
    double (*add)(int count, ...);
    double (*sub)(int count, ...);
    double (*mult)(int count, ...);
    double (*div)(int count, ...);
};

int main(){
    struct  Calculator fx;
    fx.add = &add;
    fx.sub = &sub;
    fx.mult = &mult;
    fx.div = &div;

    while(1){
    printf("Options:");
    printf("Enter '+' for addition.\n");
    printf("Enter '-' for subtraction.\n");
    printf("Enter '*' for multiplication.\n");
    printf("Enter '/' for division.\n");
    printf("Enter 'quit' to exit.\n");

    char user_input[10];
    printf("Type operator here: ");
    scanf("%s", &user_input);

    if(strcmp(user_input,"quit")==0){
        break;
    }else if (strcmp(user_input,"+")==0 || strcmp(user_input,"-")==0 || strcmp(user_input,"*")==0 || strcmp(user_input,"/")==0){
        printf("Enter numbers, separated by spaces:\n");
        int count;
        scanf("%d",&count);
        double num[count];

        for (int i = 0; i < count; i++){
            scanf("%f",&num);
        }if (strcmp(user_input,"+")==0){
            printf("Result: \n%.4f\n", fx.add(count,num[0],num[1]));
        }else if (strcmp(user_input,"-")==0){
            printf("Result: \n%.4f\n", fx.sub(count,num[0],num[1]));
        }else if (strcmp(user_input,"*")==0){
            printf("Result: \n%.4f\n", fx.mult(count,num[0],num[1]));
        }else if (strcmp(user_input,"/")==0){
            printf("Result: \n%.4f\n", fx.div(count,num[0],num[1]));
        }else{
            printf("Invalid Input\n");
        }
    }
    return 0;
}
}double add(int count, ...){
    va_list args;
    va_start(args,count);

    double total = 0;
    for (int i = 0; i < count; i++){
        total += va_arg(args, double);
    }va_end(args);
    return (float)total;
}double sub(int count, ...){
    va_list args;
    va_start(args,count);

    double result = va_arg(args,double);
    for (int i = 1; i < count; i++){
        result -= va_arg(args, double);
    }va_end(args);
    return result;
}double mult(int count, ...){
    va_list args;
    va_start(args,count);

    double product = 1;
    for (int i = 0; i < count; i++){
        product *= va_arg(args, double);
    }va_end(args);
    return product;
}double div(int count, ...){
    va_list args;
    va_start(args,count);

    double quotient = va_arg(args,double);
    for (int i = 0; i < count; i++){
        double divisor = va_arg(args, double);
        if (divisor != 0){
            quotient /= divisor;
        }else{
            printf("Cannot divide by zero!!!\n");
            return 0;
        }
    }va_end(args);
    return quotient;
}